const HtmlWebpackPlugin = require('html-webpack-plugin')
const WorkboxPlugin = require('workbox-webpack-plugin')

const path = require('path')

module.exports = {
  devtool: 'eval-source-map',
  module: {
    rules: [
      {
        test: /\.(jsx?)$/,
        exclude: /(node_modules)/,
        use: ['babel-loader'],
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
          },
        ],
      },
    ]
  },
  resolve: {
    modules: [
      'src',
      'dist',
      'node_modules'
    ]
  },
  devServer: {
    historyApiFallback: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src', 'html', 'index.html')
    }),
    new WorkboxPlugin.InjectManifest({
      swSrc: path.resolve(__dirname, 'src', 'sw.js'),
      swDest: 'sw.js',
      maximumFileSizeToCacheInBytes: 12 * 1024 * 1024,
    })
  ]
}
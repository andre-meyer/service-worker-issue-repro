/**
 * This file has no linting activated, because it uses many "magic" things from workbox.
 *
 * Please individually lint this file.
 */
/* eslint-disable */
self.__WB_DISABLE_DEV_LOGS = !(process.env.DEBUG_WORKBOX);
const SW_VERSION = '1.0.0';

console.log("sw loaded")

import {createHandlerBoundToURL, precacheAndRoute} from 'workbox-precaching';
import { NavigationRoute, registerRoute } from 'workbox-routing'

// auto generate from webpack manifest
precacheAndRoute(self.__WB_MANIFEST, {
  // Ignore all URL parameters.
  ignoreURLParametersMatching: [/.*/] // main.js is loaded with a version hash
});

self.addEventListener('message', event => {
  console.log(`[Message] event: `, event.data);
});
self.addEventListener('install', function(event) {
  event.waitUntil(self.skipWaiting());
});
self.addEventListener('activate', (event) => {
  event.waitUntil(self.clients.claim())
})

addEventListener('message', (event) => {
  console.log("message")
  if (event.data && event.data.type) {
    if (event.data.type === 'GET_VERSION') {
      event.ports[0].postMessage(SW_VERSION);
    }
  }
})

const handler = createHandlerBoundToURL('/index.html');
const navigationRoute = new NavigationRoute(handler);
registerRoute(navigationRoute);
import React, { useEffect, useState, useCallback } from 'react'
import { Workbox } from 'workbox-window';

import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

const wb = new Workbox('./sw.js');

const attachDebugEventHandlers = (events) => {
  events.forEach((eventName) => {
    wb.addEventListener(eventName, () => {
      console.log(`[serviceworker state changed] ${eventName}`);
    });
  });
}


attachDebugEventHandlers([
  'activated',
  'controlling',
  'externalactivated',
  'externalinstalling',
  'externalwaiting',
  'installed',
  'message',
  'redundant',
  'waiting',
]);


const App = () => {
  const [status, setStatus] = useState('Unknown')
  const [error, setError] = useState(null)
  const [reportedVersion, setReportedVersion] = useState(null);

  useEffect(() => {
    (async () => {
      setError(null);
      try {
        setStatus('Pending')
        const registration = await wb.register();
        setStatus('Registered')
      } catch (err) {
        setStatus('Error')
        setError(err.message)
      }
    })();
  }, [])

  const handleRequestVersion = useCallback(async () => {
    setReportedVersion(null);
    setError(null)
    setStatus("Waiting for Message")
    try {
      const version = await wb.messageSW({ type: "GET_VERSION" });
      setReportedVersion(version);
      setStatus('Message received')
    } catch (err) {
      setStatus('Message error')
      setError(err.message)
    }
  })

  return (
    <div>
      <h1>Serviceworker path issue demonstration</h1>
      <p style={{width: '600px'}}>This application is supposed to demonstrate an issue I have encountered with <code>workbox-window</code> where the registraton will be reported but no messages ever resolve and no events get triggered. You can see this behaviour by clicking the "request version" button, clicking the "deep path" link, reloading and trying again. It will be stuck in "waiting for message" forever</p>
      <p>Serviceworker Status is {status} {error}</p>
      <button type="button" onClick={handleRequestVersion}>Request Version</button>
      {reportedVersion && <p>Serviceworker reported version {reportedVersion}</p>}
      <hr />
      <Router basename="/">
        <ul style={{listStyle: 'none', padding: 0}}>
          <li>
            <Link to='/'>Home</Link>
          </li>
          <li>
            <Link to='/my-path'>Normal Path</Link>
          </li>
          <li>
            <Link to='/my-path/deep-path'>Deep Path</Link>
          </li>
        </ul>
        <Route path="/my-path">
          <p>This should work</p>
        </Route>
        <Route path="/my-path/deep-path">
          <p>This should too but doesn't, when you reload on this page</p>
        </Route>
      </Router>
      <hr />
      <a href="https://stackoverflow.com/questions/62427020" target="_blank" rel="noreferrer noopener">Stackoverflow Post</a> -&nbsp;
      <a href="https://gitlab.com/andre-meyer/service-worker-issue-repro" target="_blank" rel="noreferrer noopener">Source on Gitlab</a>
    </div>
  )
}

export default App